#include<algorithm>
#include<iostream>
#include<string>
#include<vector>
#include<sstream>

#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include "TH1.h"
#include "TH1F.h"
#include "TCanvas.h"

using namespace std;

/*******************************************************************************************/
/* SAMPLING WITHOUT REPLACEMENT ************************************************************/
/*******************************************************************************************/

/*
helper for swr
*/

void printSWR(char set[], string prefix, int n, int k, vector< vector<int> > &idxes){
	 
	//base case
	if(k == 0){
		
		vector<int> samplIdx;
		stringstream ss(prefix);
		string buf;
		int idx;
		while(ss >> buf){
			istringstream(buf) >> idx;
			samplIdx.push_back(idx);
		}
		idxes.push_back(samplIdx);
	
		return;
	}

	for(int i = 0; i < n; ++i){
		
		string newPrefix = prefix + ' ' + set[i];
		
		printSWR(set, newPrefix, n, k-1, idxes);

	}
}

/*
Takes population 1D vector. Takes sample size. Return ALL possible samples SWR.
!WARNING! - NEED TO BE GENERALISED FOR POPULATION WITH MORE THAN 6 ELEMENTS!!!
*/

void swr(vector<int> population, int n, vector< vector<int> > &samples){

	
	char setPOP[] = {'0','1','2','3','4','5'}; //!GENERALIZE!
	int N = population.size();
	vector< vector<int> > idxes;
	
	printSWR(setPOP, "", N, n, idxes);

	for(int i = 0; i < idxes.size(); ++ i){
 		vector<int> sample;
                for(int j = 0; j < idxes[i].size(); ++j){
                        sample.push_back(population[idxes[i][j]]);
                }
		samples.push_back(sample);

        }	
 
}

/*******************************************************************************************/
/* SAMPLING WITH REPLACEMENT ***************************************************************/
/*******************************************************************************************/

/*
Takes population (1d vector), sample size and returnes ALL possible SWOR samples
*/

template<typename T, typename A>
void comb(vector<T> population, int sampleSize, vector< vector<A> > &samples){

	int N = population.size();
	int k = sampleSize;
	
	//filler constructor
	//string (size_t n, char c);
	//Fills the string with n consecutive copies of character c.
	//array of 11111..1
	string mask(k,1); 

	//resizeing
	//If n is greater than the current string length, the current content is extended by 
	//inserting at the end as many characters c as needed to reach a size of n.
	//void resize (size_t n, char c);
	//array of 1111..1000..0 (k ones, N - k zeros)
	mask.resize(N,0);


	//now we have array of length N. There is k ones and (N-k) zeros.
	//Using permutatuin we will move ones in all possible positions and that is how we choose samples
	do{
		
		vector<A> sample;

		for (int i = 0; i < N; ++i){
			if(mask[i]){
				sample.push_back(population[i]);
			}
		}
		
		samples.push_back(sample);



	}while(prev_permutation(mask.begin(), mask.end()));

}

/*******************************************************************************************/
/* FUNCTIONS FOR PRINTING ******************************************************************/
/*******************************************************************************************/

template<typename T>
void printSamples(vector< vector<T> > samples){

	for(int i = 0; i < samples.size(); ++ i){

		for(int j = 0; j < samples[i].size(); ++j){
			cout<<samples[i][j]<<" ";
		}

		cout<<endl;

	}

}


template<typename T>
void printSamples(vector<T> samples){

	for(int i = 0; i < samples.size(); ++ i){

		cout<<samples[i]<<" ";
		

	}

	cout<<endl;

}

/*******************************************************************************************/
/* MEANS FOR EVERY VECTOR IN GIVEN VECTOR  *************************************************/
/*******************************************************************************************/

/*
Given vector of vectors calcultes means for every vector
Returnes vector of means.
*/
template<typename T>
void calculateSamplesMean(vector< vector<T> > samples, vector<double> &samplesMean){

	double mean;

	for(int i = 0; i < samples.size(); ++ i){

		mean = 0;

		for(int j = 0; j < samples[i].size(); ++j){
			mean = mean + samples[i][j];
		}

		mean = mean / samples[i].size();

		samplesMean.push_back(mean);


	}

}

/*******************************************************************************************/
/* MEAN AND STDDEV FOR GIVEN VECTOR ********************************************************/
/*******************************************************************************************/

/*
Given vector of elements (1d) returnes mean and standard deviation (biased!)
*/
template<typename T>
void calcMeanStddev(vector<T> elems, double &mean, double &stddev){
	
	mean = 0;
	for(int i = 0; i < elems.size(); ++i){
		mean = mean + elems[i];
	}
	mean = mean / elems.size();

        double variance = 0;
	for(int i = 0; i < elems.size(); ++i){
		variance = variance + pow((elems[i]-mean) ,2);
	}
	variance = variance / (elems.size());
	stddev = sqrt(variance);

}

/*******************************************************************************************/
/* PLOTTING HISTOGRAM USING ROOT ***********************************************************/
/*******************************************************************************************/

/*
Give vector of elements, name of canvas and histogram
Plots a histogram
*/
template<typename T>
void plotHist(vector<T> elems, string canvasStr, string histogramStr){

	sort(elems.begin(), elems.end());
	int minElem = elems[0];
	int maxElem = elems[elems.size()-1];

	TCanvas* canvas = new TCanvas(canvasStr.c_str(), histogramStr.c_str(), 640, 480);
	TH1F *hist = new TH1F(histogramStr.c_str(), histogramStr.c_str(), 100, minElem-5, maxElem+5);
	
	hist->SetFillColor(3);
	hist->Draw();

	for(int i = 0; i < elems.size(); i++){
		hist->Fill(elems[i]);
	} 
	canvas->Modified();
	canvas->Update();
}

/*
Give histogram in form of vector!, name of canvas and histogram
Plots a histogram
*/
template<typename T>
void plotBars(vector<T> bars, string canvasStr, string histogramStr){

        int start = 1;
        int end = bars.size();

	for(int i = 0; i < bars.size(); ++i){
		cout<<bars[i]<<" ";
	}
	cout<<endl;

        TCanvas* canvas = new TCanvas(canvasStr.c_str(), histogramStr.c_str(), 640, 480);
        TH1F *hist = new TH1F(histogramStr.c_str(), histogramStr.c_str(), bars.size(), 0, bars.size());

        hist->SetFillColor(3);
        hist->Draw();

        for(int i = 0; i < bars.size(); i++){
                hist->AddBinContent(i+1,bars[i]);
        }
	canvas->Modified();
        canvas->Update();

}


/*******************************************************************************************/
/* <SPECIALIZED> CALCULATE SDOM FROM POPULATION AND GIVEN SAMPLE SIZE AND FOR ALL POSSIBLE SAMPLES*/
/*******************************************************************************************/

/*
Takes population, sample size and canvas name.
Calculate SDOM from all possible samples (SWR) of size n and plots result
*/
template<typename T>
void calcPlotSDOM(vector<T> population, int n, string canvas, string histogram){

        //drawing samples
        vector< vector<int> > samples;
        swr(population, n, samples);

        //calculating means from samples
        vector<double> SDOM;
        calculateSamplesMean(samples, SDOM);

        //calculating mean and stddev of SDOM
        double meanSDOM, stddevSDOM;
        calcMeanStddev(SDOM, meanSDOM, stddevSDOM);
        cout<<"MEAN_SDOM="<<meanSDOM<<endl;
        cout<<"STDDEV_SDOM="<<stddevSDOM<<endl;

	//plotting SDOM pdf
	plotHist(SDOM, canvas, histogram);

}

/*******************************************************************************************/
/* SAMPLING SAMPLES ************************************************************************/
/*******************************************************************************************/

/* preforms sampling of all possible samples, calculated SDOM, mean and stddev of SDOM 
   for subset
   RETURNS: mean and stddev of SDOM created from subset of all samples
*/
template<typename T>
void sampleSamples(vector<T> population, int n, int nS, double &mean, double &stddev){

	//draw all possible samples
	vector <vector<T> > samples;
	swr(population, n, samples);
	
	//sample samples
	vector<int> sampleIndex;
	vector< vector<T> > sampledSample;
	//generate nS indexes (without) replacement
	while(1){
		
		if(sampleIndex.size() >= nS){
			break;
		}

		int tempIdx = rand() % samples.size(); //from 0 to sample.size()-1

		bool contains = false;
		for(int i = 0; i < sampleIndex.size(); i++){
			if(tempIdx == sampleIndex[i]){
				contains = true;
				break;
			}
		}
		if(!contains){
			sampleIndex.push_back(tempIdx);
			sampledSample.push_back(samples[tempIdx]);
		}
		
		
	}

	//calculate SDOM for sampled sample
	vector<double> SDOM;
	calculateSamplesMean(sampledSample, SDOM);

	//calculated mean and std dev from SDOM
	calcMeanStddev(SDOM, mean, stddev);
	
}

/* compareWith: 1 stddev and mean, 2 stddev, 3 mean 
   nS - number of all possible samples from population
RETURNS: plots for every samplesize number of results in range
*/
template<typename T>
void testRange(vector<T> population, int n, int nS, double trueMean, double trueStddev, double offset, int compareWith){

	vector<int> inRange;

        double lowerStddev = trueStddev - offset * trueStddev;
        double upperStddev = trueStddev + offset * trueStddev;
        double lowerMean = trueMean - offset * trueMean;
        double upperMean = trueMean + offset * trueMean;

        for(int noSampl = 1; noSampl <= nS; noSampl++){
                int cntRange = 0;
                for(int i = 0; i < 1000; ++i){
                        double mean, stddev;
                        sampleSamples(population, n, noSampl, mean, stddev);
                        if(compareWith == 1 && (mean >= lowerMean && mean <= upperMean) && (stddev >= lowerStddev && stddev <= upperStddev)){
                                cntRange++;
                        }
			if(compareWith == 2 && (stddev >= lowerStddev && stddev <= upperStddev)){
				cntRange++;
                        }
			if(compareWith == 3 && (mean >= lowerMean && mean <= upperMean)){
				cntRange++;
			}
                }

                inRange.push_back(cntRange);
        }

	//plotting results
	string name = to_string(trueStddev) + " " + to_string(trueMean) + " " + to_string(offset);
	plotBars(inRange, name.c_str() , name.c_str());

}

/*******************************************************************************************/
/* MAIN ************************************************************************************/
/*******************************************************************************************/
	
void sampling(){

	srand(time(NULL));

	/*************************************************************************/
	/*	POPULATION	**************************************************/

	//generating population
	int arr[] = {2, 4, 5, 9, 11, 14};
	int N = 6;
	vector<int> population;
	for(int i = 0; i < N; ++i){
		population.push_back(arr[i]);
	}
	
	//population mean and stddev
	double meanPOP, stddevPOP;
	calcMeanStddev(population, meanPOP, stddevPOP);
	cout<<"MEAN_POP="<<meanPOP<<endl;
	cout<<"STDDEV_POP="<<stddevPOP<<endl;

	//population pdf
	plotHist(population, "canvasPOP", "histPOP");

	/******************************************************************************/
	/*	ALL POSSIBLE SWR, n = 2	**************************************/
	/****************************************************************************/
	calcPlotSDOM(population, 2, "canvas SDOM n=2", "hist SDOM n=2");

	/******************************************************************************/
        /*	ALL POSSIBLE SWR, n = 3        **************************************/
        /****************************************************************************/
 	calcPlotSDOM(population, 3, "canvas SDOM n=3", "hist SDOM n=3");

	/******************************************************************************/
        /*	SAMPLING ALL POSSIBLE SWR, n = 2       ******************************/
        /****************************************************************************/
	//range 1%
	testRange(population, 2, 36, 7.5, 2.965, 0.01, 1);
	//range 3%
	testRange(population, 2, 36, 7.5, 2.965, 0.03, 1);

	/******************************************************************************/
        /*      SAMPLING ALL POSSIBLE SWR, n = 3       ******************************/
        /****************************************************************************/
        //range 1%
        testRange(population, 3, 216, 7.5, 2.421, 0.01, 1);
        //range 3%
	testRange(population, 3, 216, 7.5, 2.421, 0.03, 1);

	 

}


int main(){

 sampling();
	
}

